/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

#pragma once
#include <tchar.h>
#include <atlconv.h>

#include <string>
#include <map>
#include <vector>
using namespace std;

#define XP_WIN
#define STATIC_JS_API

#include <jsapi.h>

class CJsManager;
class CJsArray
{
public:
	CJsArray(CJsManager *pMgr);
	~CJsArray(void);

public:
	jsval *getArrayData();
	unsigned int getArrayLength();
	void pushElement(int obj);
	void pushElement(double obj);
	bool pushElement(LPCTSTR obj);
	bool FormatElements(TCHAR *fmt, ...);

private:
	vector <jsval> m_arrVal;
	CJsManager *m_jsMgr;
};

/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////

class CJsManager
{
public:
	CJsManager(void);
	~CJsManager(void);

public:
	bool initGlobalContext(unsigned int contextSize = 8L * 1024, unsigned int runTimeSize = 8L * 1024L * 1024L);

	bool runScriptString(LPCTSTR scriptString, jsval& retVal);
	bool runScriptFile(LPCTSTR jsPath, jsval& retVal);

	bool evalFunction(LPCTSTR funName, CJsArray* paramArray, jsval& retVal);
	bool evalObjFunction(LPCTSTR objName, LPCTSTR funName, CJsArray* paramArray, jsval& retVal);

	bool jsval_to_int(jsval vp, int* outval);
	bool jsval_to_uint(jsval vp, unsigned int* outval);
	bool jsval_to_ushort(jsval vp, unsigned short* outval);
	bool jsval_to_cstring_for_delete(jsval vp, char** outval);
	bool jsval_to_string(jsval vp, string &outval);

public:
	JSRuntime *m_rt;
	JSContext *m_ctx;
	JSObject *m_gObj;
	string m_lastErrorString;

	bool m_bInitOk;
};

typedef map<JSContext*,CJsManager*> Ctx_Map;

/////////////////////////////////////////////////////////////
// 声明：本源码来自 vc驿站：http://www.cctry.com
// C、C++、VC++ 各种学习资源，免费教程，期待您的加入！
/////////////////////////////////////////////////////////////