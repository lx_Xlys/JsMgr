#JsMgr

项目简介：对FireFox火狐浏览器的JavaScript脚本引擎做的二次封装，封装成类库JsMgr，供C++程序调用。
方便在C++中进行与JavaScript脚本代码的交互，调用JavaScript脚本中的相关函数，或者对象的函数等等。